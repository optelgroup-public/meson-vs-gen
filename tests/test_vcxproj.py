import filecmp

import lxml.etree as ET

from vsgen.api import Generator
from vsgen.vcxproj import VcxProj


def test_read_write_vcxproj(mp_source_path, mp_build_path, tmp_path):
    gen = Generator(tmp_path, mp_source_path, "")
    gen.set_config(mp_build_path, "debug")
    prj1 = gen.project("mesonproject").write()

    prj2 = VcxProj("mesonproject", tmp_path)
    prj2.load()
    prj2.name = "p2"
    prj2.write()

    assert filecmp.cmp(prj1.filepath, prj2.filepath)


def test_paths_are_relative_to_project(mp_source_path, mp_build_path, tmp_path):
    gen = Generator(tmp_path, mp_source_path, "")
    gen.set_config(mp_build_path, "debug")
    prj = gen.project("mesonproject").write()

    project_xml = ET.parse(prj.filepath).getroot()

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}BuildDir'):
        assert (prj.path / node.text).resolve() == mp_build_path

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}NMakeOutput'):
        assert (prj.path / node.text).resolve().parent == mp_build_path

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}NMakeIncludeSearchPath'):
        for p in node.text.split(';'):
            assert (prj.path / p).is_dir()

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}None'):
        assert (prj.path / node.get("Include")).is_file()

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}ClInclude'):
        assert (prj.path / node.get("Include")).is_file()

    for node in project_xml.iterfind(f'.//{{{prj.NS}}}ClCompile'):
        assert (prj.path / node.get("Include")).is_file()
