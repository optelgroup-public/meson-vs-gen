from pathlib import Path
import subprocess

import pytest

@pytest.fixture(scope='session')
def mp_source_path() -> Path:
    return Path(__file__).absolute().parent / 'mesonproject'


@pytest.fixture(scope='session')
def mp_build_path(mp_source_path: Path, tmp_path_factory: pytest.TempPathFactory) -> Path:
    build_dir = tmp_path_factory.mktemp('build')
    subprocess.run(['meson', 'setup', '-Dbuildtype=debug', str(build_dir), str(mp_source_path)])
    return build_dir


@pytest.fixture(scope='session')
def vs_yaml_path(mp_source_path: Path, mp_build_path: Path, tmp_path_factory: pytest.TempPathFactory) -> Path:
    config_dir = tmp_path_factory.mktemp('config')
    
    vs_yaml = (mp_source_path / 'vs.yaml').read_text(encoding='utf-8')
    vs_yaml = vs_yaml.replace('build:', f"'{mp_build_path}':")

    vs_yaml_file = config_dir / 'vs.yaml'
    vs_yaml_file.write_text(vs_yaml, encoding='utf-8')

    return vs_yaml_file


def pytest_addoption(parser):
    parser.addoption("--fast", action="store_true", default=False, help="skip slow tests")


def pytest_configure(config):
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--fast"):
        skip_slow = pytest.mark.skip(reason="--fast option was given")
        for item in items:
            if "slow" in item.keywords:
                item.add_marker(skip_slow)
