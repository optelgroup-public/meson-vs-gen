import filecmp

from vsgen.api import Generator
from vsgen.sln import SolutionFile


def test_read_write_sln(mp_source_path, mp_build_path, tmp_path):
    gen = Generator(tmp_path, mp_source_path, "")
    gen.set_config(mp_build_path, "debug")
    prj = gen.project("mesonproject", update=False).write()
    sln1 = gen.solution("solution", update=False)
    sln1.add_project(prj, build_solution_target=True)
    sln1.write()

    sln2 = SolutionFile(sln1.path)
    sln2.load_configs()
    sln2.path = sln2.path.with_stem('solution2')
    sln2.write()

    assert filecmp.cmp(sln1.path, sln2.path)
