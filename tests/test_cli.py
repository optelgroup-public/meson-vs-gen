from pathlib import Path
import re

from cleo.testers.command_tester import CommandTester
import pytest

from vsgen.cli._pathconfig import PathConfig
from vsgen.cli import GenerateCommand, ProjectCommand, SolutionCommand


def _fetch_fixtures(value, request):
    if not isinstance(value, str):
        return value

    def subs(m):
        return str(request.getfixturevalue(m[1]))

    return re.sub(r"\{([a-z_]+)\}", subs, value)


@pytest.mark.parametrize("name", ["Project", "Project.vcxproj", "subdir/Project", "subdir/Project.vcxproj"])
@pytest.mark.parametrize("sourcedir", [None, "{mp_source_path}"])
@pytest.mark.parametrize("builddir", ["{mp_build_path}", "config:{mp_build_path}"])
def test_path_config(monkeypatch, request, tmp_path, name, sourcedir, builddir, mp_source_path, mp_build_path):
    monkeypatch.chdir(mp_source_path)

    sourcedir = _fetch_fixtures(sourcedir, request)
    projectdir = Path(name).parent
    if projectdir == Path("."):
        projectdir = False

    builddirs = [_fetch_fixtures(builddir, request)]
    build_config = builddir.split(":")[0] if ":" in builddir else None

    outputdir = str(tmp_path)

    pc = PathConfig(name, sourcedir, builddirs, outputdir)

    assert pc.name == Path(name).stem
    assert pc.source_path == mp_source_path
    assert pc.build_paths == {mp_build_path: build_config}
    assert pc.output_path == tmp_path
    assert pc.project_dir == projectdir


@pytest.mark.parametrize("name", ["Project", "Project.vcxproj", "subdir/Project", "subdir/Project.vcxproj"])
def test_path_config_no_outputdir(monkeypatch, name, mp_source_path, mp_build_path):
    monkeypatch.chdir(mp_source_path)

    pc = PathConfig(name, str(mp_source_path), [str(mp_build_path)], outputdir=None)

    assert pc.name == Path(name).stem
    assert pc.source_path == mp_source_path
    assert pc.build_paths == {mp_build_path: None}
    assert pc.output_path == mp_source_path / Path(name).parent
    assert pc.project_dir is False


@pytest.mark.parametrize(
    "args, result_code",
    [
        # source is file:
        ("--sourcedir {mp_source_path}\\meson.build --builddir {mp_build_path} --outputdir {tmpdir} Project", 1),
        # source does not exists:
        ("--sourcedir {tmpdir}\\foobar --builddir {mp_build_path} --outputdir {tmpdir} Project", 1),
        # no builddir
        ("--sourcedir {mp_source_path} --outputdir {tmpdir} Project", 2),
        # project not relative to output:
        ("--sourcedir {mp_source_path} --builddir {mp_build_path} --outputdir {tmpdir} C:\\Project", 1),
        # ok!
        ("--sourcedir {mp_source_path} --builddir {mp_build_path} --outputdir {tmpdir} Project", 0),
    ],
)
def test_invalid_arguments(monkeypatch, request, args, result_code):
    cmd = CommandTester(ProjectCommand())
    assert cmd.execute(_fetch_fixtures(args, request)) == result_code


def test_generate_project(mp_source_path, mp_build_path, tmp_path):
    args = f"--sourcedir {mp_source_path} --builddir {mp_build_path} --outputdir {tmp_path} mesonproject"

    cmd = CommandTester(ProjectCommand())
    result = cmd.execute(args)

    assert result == 0
    assert (tmp_path / "mesonproject.vcxproj").exists()
    assert (tmp_path / "mesonproject.vcxproj.filters").exists()
    assert (tmp_path / "mesonproject.vcxproj.user").exists()


def test_generate_solution(mp_source_path, mp_build_path, tmp_path):
    args = (
        f"--sourcedir {mp_source_path} --builddir {mp_build_path} "
        f"--outputdir {tmp_path} --projectdir projects mesonproject"
    )

    cmd = CommandTester(SolutionCommand())
    result = cmd.execute(args)

    assert result == 0
    assert (tmp_path / "mesonproject.sln").exists()
    assert (tmp_path / "projects" / "mesonproject.vcxproj").exists()
    assert (tmp_path / "projects" / "mesonproject.vcxproj.filters").exists()
    assert (tmp_path / "projects" / "mesonproject.vcxproj.user").exists()
    assert (tmp_path / "projects" / "ALL.vcxproj").exists()
    assert (tmp_path / "projects" / "ALL.vcxproj.filters").exists()
    assert (tmp_path / "projects" / "ALL.vcxproj.user").exists()


def test_generate_from_conf(tmp_path, mp_source_path, vs_yaml_path):
    args = f"--sourcedir {mp_source_path} --outputdir {tmp_path} {vs_yaml_path}"

    cmd = CommandTester(GenerateCommand())
    result = cmd.execute(args)

    assert result == 0
    assert (tmp_path / "Sol.sln").exists()
    assert (tmp_path / "Sol.gta.runsettings").exists()
    assert (tmp_path / "prjs" / "mesonprj" / "mesonprj.vcxproj").exists()
    assert (tmp_path / "prjs" / "mesonprj" / "mesonprj.vcxproj.filters").exists()
    assert (tmp_path / "prjs" / "mesonprj" / "mesonprj.vcxproj.user").exists()
